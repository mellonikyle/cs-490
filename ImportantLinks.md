# Here are some important links that may come in handy!

#   Important GitLab Links: 

1. Libre Food Pantry group: https://gitlab.com/LibreFoodPantry
2. BEAR-Necessities-Market Repository: https://gitlab.com/LibreFoodPantry/modules/usermodule-bnm/BNM
3. This Class: https://gitlab.com/heidijcellis/cs-490/-/tree/master/

#   Other Important Links:

1. Libre Food Pantry current website: http://librefoodpantry.org/#/
2. Libre Food Pantry new website (WIP): https://librefoodpantry.gitlab.io/website/
3. Permanent Class discord invite link: https://discord.gg/hjguQV
4. Permanent LFP discord invite link: https://discord.com/invite/PRth8YK
5. Google Drive share folder: https://drive.google.com/drive/folders/1jP0VEHHanBm0gEHtAzXrUsb4zaEcL1TJ

